import plotly.express as px
import pandas as pd
import sys

df = pd.read_csv('../dataset/diabetes.csv')
del df['id']
all_dimensions = [
            'Pregnancies',
            'Glucose',
            'BloodPressure',
            'SkinThickness',
            'Insulin',
            'BMI',
            'DiabetesPedigreeFunction',
            'Age'
            ]
dimensions = all_dimensions if len(sys.argv) <= 1 else sys.argv[1:]
fig = px.scatter_matrix(df,
        title="Scatter matrix of the diabetes data set",
        dimensions=dimensions,
        color="Outcome"
        )
fig.update_traces(diagonal_visible=False)
fig.show()
