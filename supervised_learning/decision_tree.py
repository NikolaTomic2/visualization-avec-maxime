from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn import metrics
import csv

def get_data():
    data = []
    target = []
    with open('../dataset/diabetes.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in list(csvReader)[1:]:
            data.append([float(x) for x in row[:-1]])
            target.append(int(row[-1]))
    return data, target


def diabetes_model():
    data, target = get_data()
    data_train, data_test, target_train, target_test = train_test_split(
        data, target, test_size = 0.4, random_state=1
    )
    clf = tree.DecisionTreeClassifier()
    clf.fit(data_train, target_train)
    target_pred = clf.predict(data_test)
    # Finding accuracy by comparing actual response values(y_test)with predicted response value(y_pred)
    print("Accuracy:", metrics.accuracy_score(target_test, target_pred))
    # Providing sample data and the model will make prediction out of that data

    sample = data[:10]
    preds = clf.predict(sample)
    print("Predictions:", preds)


diabetes_model()
