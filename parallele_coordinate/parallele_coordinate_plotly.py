import plotly.express as px
import pandas as pd
import sys


all_dimensions = [
            'Pregnancies',
            'Glucose',
            'BloodPressure',
            'SkinThickness',
            'Insulin',
            'BMI',
            'DiabetesPedigreeFunction',
            'Age',
            'Outcome'
            ]
dimensions = all_dimensions if len(sys.argv) <= 1 else sys.argv[1:]
df = pd.read_csv('../dataset/diabetes.csv')
fig = px.parallel_coordinates(df,
                              color="Outcome",
                              dimensions=dimensions,
                              color_continuous_scale=px.colors.diverging.Tealrose,
                              color_continuous_midpoint=0.5)
fig.show()
# fig.write_image("parallel_coordinate_plot.pdf")
